# simpleopenaiapi



## whats this

简单对openai提供的新api进行调用，基于[官方示例](https://platform.openai.com/docs/quickstart/build-your-application) 简单修改

## 如何运行

参照上述链接中的步骤搭建环境即可，需要注意的地方：
* requirements中openai版本需改为0.27.0，因为只有0.27版本才支持最新的gpt-3.5-turbo模型
